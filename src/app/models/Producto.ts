/*export class Producto {
    _id:String;
    name: String;
    categoria: [String];
    cantidad: Number;
    costo_venta: Number;
    costo_compra: Number;
    proveedor : [String];

    constructor(_id:String, name:String, categoria: [String],
        cantidad: Number, costo_venta: Number, costo_compra: Number, proveedor: [String]){
        this._id = _id;
        this.name = name;
        this.categoria = categoria;
        this.cantidad = cantidad;
        this.costo_venta = costo_venta;
        this.costo_compra = costo_compra;
        this.proveedor = proveedor;
    }
}*/

export interface Producto {
    _id:String;
    name: String;
    categoria: [String];
    cantidad: Number;
    costo_venta: Number;
    costo_compra: Number;
    proveedor : [String];
}