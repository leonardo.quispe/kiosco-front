export class Categoria{
    id: Number;
    nombre: String;

    constructor(id:Number, nombre:String){
        this.id = id;
        this.nombre = nombre;
    }

}