export class Compra {
    _id: String;
    fecha: Date;
    valor_total: Number;
    productos: [String];
    constructor(_id:String,fecha:Date, valor_total:Number, productos: [String]){
        this._id = _id
        this.fecha = fecha;
        this.valor_total = valor_total;
        this.productos = productos;
    }
}