/*export class Proveedor {
    _id:String;
    nombre: String;
    direccion: String;
    telefono: String;
    ruc: String;

    constructor(_id:String, nombre: String, direccion: String,
        telefono: String, ruc: String){
        this._id =_id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.ruc = ruc;
    }
}*/

export interface Proveedor {
    _id:String;
    nombre: String;
    direccion: String;
    telefono: String;
    ruc: String;
}