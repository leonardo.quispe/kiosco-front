export class User {
    id: Number;
    username: String;
    email: String;
    password: String;
    roles: [String];

    constructor(id: Number, username: String, email: String, password:String, roles: [String]){
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }
}