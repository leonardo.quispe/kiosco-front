import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Proveedor } from '../models/Proveedor';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  token = localStorage.getItem('token')
  url = 'http://localhost:4000/api/proveedor/';
  constructor(private http: HttpClient) { }

  getProveedores(): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.get(this.url, options);
  }

  eliminarProveedor(id: String): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.delete(this.url + id, options);
  }

  guardarProveedor(proveedor: any): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.post(this.url, proveedor, options);
  }

  editarProveedor(proveedor:any, id:String): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.put(this.url + id, proveedor, options)
  }

  obtenerProveedor(id: string): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.get(this.url + id, options);
  }
}