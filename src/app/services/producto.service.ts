import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Producto } from '../models/Producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  token = localStorage.getItem('token')
  url = 'http://localhost:4000/api/producto/';
  constructor(private http: HttpClient) { }

  getProductos(): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.get(this.url, options);
  }

  eliminarProducto(id: String): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.delete(this.url + id, options);
  }

  guardarProducto(producto: any): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.post(this.url, producto, options);
  }
  editarProducto(producto: any, id: String): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.put(this.url + id, producto, options);
  }

  obtenerProducto(id: String): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.get(this.url + id, options);
  }
}
