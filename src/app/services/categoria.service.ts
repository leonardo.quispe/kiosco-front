import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Categoria } from "../models/Categoria";

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  token = localStorage.getItem('token')
  url = 'http://localhost:4000/api/categoria/';
  constructor(private http: HttpClient) { }

  getCategoria(): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.get(this.url, options);
  }

  eliminarCategoria(id: string): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.delete(this.url + id, options);
  }

  guardarCategoria(categoria: Categoria): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.post(this.url, categoria, options);
  }

  obtenerCategoria(id: string): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.get(this.url + id, options);
  }
}
