import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompraService {

  token = localStorage.getItem('token')
  url = 'http://localhost:4000/api/compra/';
  constructor(private http: HttpClient) { }

  getCompras(): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.get(this.url, options);
  }

  guardarCompra(formcompra: any): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.post(this.url, formcompra, options);
  }

  obtenerCompra(id:String): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.get(this.url+id, options)
  }
  
}