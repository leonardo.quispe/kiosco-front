import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Proveedor } from '../models/Proveedor';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  token = localStorage.getItem('token')
  url = 'http://localhost:4000/api/venta/';
  constructor(private http: HttpClient) { }

  getVentas(): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.get(this.url, options);
  }

  guardarVenta(formventa: any): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.post(this.url, formventa, options);
  }

  obtenerVenta(id:String): Observable<any> {
    let options = {
      headers: new HttpHeaders({
        "x-access-token" : this.token!
      })
    }
    return this.http.get(this.url+id, options)
  }
  
}