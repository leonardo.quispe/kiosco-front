import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CompraComponent } from './components/compra/compra.component';
import { VentaComponent } from './components/venta/venta.component';
import { InventarioComponent } from './components/inventario/inventario.component';
import { NuevoComponent } from './components/nuevo/nuevo.component';
import { NuevoProvComponent } from './components/nuevoProv/nuevoProv.component';
import { EditarProductoComponent } from './components/editarProducto/editarProducto.component';
import { EditarProveedorComponent } from './components/editarProveedor/editarProveedor.component';
import { VisualCompraComponent } from './components/visualCompra/visualCompra.component';
import { VisualVentaComponent } from './components/visualVenta/visualVenta.component';

const APP_ROUTES: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'compra', component: CompraComponent },
  { path: 'venta', component: VentaComponent },
  { path: 'inventario', component: InventarioComponent },
  { path: 'nuevoProducto', component: NuevoComponent },
  { path: 'nuevoProveedor', component: NuevoProvComponent },
  { path: 'editarProducto/:id', component: EditarProductoComponent },
  { path: 'editarProveedor/:id', component: EditarProveedorComponent },
  { path: 'compraCompleta/:id', component: VisualCompraComponent },
  { path: 'ventaCompleta/:id', component: VisualVentaComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});