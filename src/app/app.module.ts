import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { MatTabsModule } from '@angular/material/tabs';

// Rutas
import { APP_ROUTING } from './app.routes';

// Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CompraComponent } from './components/compra/compra.component';
import { VentaComponent } from './components/venta/venta.component';
import { InventarioComponent } from './components/inventario/inventario.component';
import { NuevoComponent } from './components/nuevo/nuevo.component';
import { NuevoProvComponent } from './components/nuevoProv/nuevoProv.component';
import { EditarProductoComponent } from './components/editarProducto/editarProducto.component';
import { EditarProveedorComponent } from './components/editarProveedor/editarProveedor.component';
import { VisualCompraComponent } from './components/visualCompra/visualCompra.component';
import { VisualVentaComponent } from './components/visualVenta/visualVenta.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FiltroPipe } from './pipes/filtro.pipe';
import { Filtro2Pipe } from './pipes/filtro2.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CompraComponent,
    VentaComponent,
    InventarioComponent,
    NuevoComponent,
    NuevoProvComponent,
    EditarProductoComponent,
    EditarProveedorComponent,
    VisualCompraComponent,
    VisualVentaComponent,
    FiltroPipe,
    Filtro2Pipe    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    APP_ROUTING,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTabsModule
  ],
  exports: [
    MatTabsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
