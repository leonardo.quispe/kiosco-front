import { Pipe, PipeTransform } from '@angular/core';
import { Producto } from '../models/Producto';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(valores: any[], page: number = 0, search: string = '', id: number ): any {

    if( search.length === 0)
      return valores.slice(page, page+5);

    if(id === 0 ){
      const filterData = valores.filter( data => data.name.includes( search )
      || data.categoria.toString().includes( search )
      || data.proveedor.toString().includes(search ) );
      return filterData.slice(page, page+5);
    }else if (id === 1){
      const filterData = valores.filter( data => data.nombre.includes( search )
      || data.direccion.toString().includes( search )
      || data.telefono.toString().includes( search ) );
      return filterData.slice(page, page+5);
    }else if (id === 2){
      const filterData = valores.filter( data => data.fecha.toString().includes( search )
      || data.valor_total.toString().includes( search ) );
      return filterData.slice(page, page+5);
    }else if (id === 3){
      const filterData = valores.filter( data => data.fecha.toString().includes( search )
      || data.valor_total.toString().includes( search ) );
      return filterData.slice(page, page+5);
    }else{
      return []
    }
  
  }

}
