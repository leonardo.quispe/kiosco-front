import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro2'
})
export class Filtro2Pipe implements PipeTransform {

  transform(valores: any[], page: number = 0): any {
    return valores.slice(page, page+5);
  }

}
