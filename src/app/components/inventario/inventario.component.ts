import { Component, NgModule, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { Producto } from '../../models/Producto';
import { ProductoService } from 'src/app/services/producto.service';
import { ProveedorService } from 'src/app/services/proveedor.service';
import { Proveedor } from 'src/app/models/Proveedor';
import { User } from 'src/app/models/User'
import { CursorError } from '@angular/compiler/src/ml_parser/lexer';
import { resolveSanitizationFn } from '@angular/compiler/src/render3/view/template';
import { Rol } from 'src/app/models/Rol';
import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y/input-modality/input-modality-detector';
import { Venta } from 'src/app/models/Venta';
import { VentaService } from 'src/app/services/venta.service';
import { CompraService } from 'src/app/services/compra.service';
import { Compra } from 'src/app/models/Compra';
import { MatTabsModule } from '@angular/material/tabs';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.css'],
  providers: []
})
export class InventarioComponent implements OnInit {
  listProductos: Producto[] = [];
  listProveedores: Proveedor[] = [];
  listRoles: Rol[] = [];
  listVentas: Venta[] = [];
  listCompras: Compra[] = [];
  select:any = 0;
  page:number = 0;
  search: string = '';

  constructor( private productoService: ProductoService,
    private proveedorService: ProveedorService,
    private vs: VentaService,
    private cs: CompraService,
    private router:Router ) { }

  ngOnInit(): void{
    this.select = Number(localStorage.getItem("i"))
    this.obtenerProductos();
    this.obtenerProveedor();
    this.obtenerVentas();
    this.getCurrentUserRol();
    this.obtenerCompras();
  }

  nextPage(){
    this.page += 5;
  }

  prevPage(){
    if( this.page >0 )
     this.page -= 5;
  }

  onSearch(search: string){
    this.page = 0;
    this.search = search;
  }

  change(index:any){
    console.log(index)
  }

  nuevoProd() {
    this.router.navigate( ['/nuevoProducto'] );
    localStorage.setItem("i", String("0") )
  }
  nuevoProv() {
    this.router.navigate( ['/nuevoProveedor'] );
    localStorage.setItem("i", String("1") )
  }
  indexProducto(){
    localStorage.setItem("i", String("0"))
  }
  indexProveedor(){
    localStorage.setItem("i", String("1"))
  }

  indexCompra(){
    localStorage.setItem("i", String("2") )
  }

  indexVenta(){
    localStorage.setItem("i", String("3") )
  }

  
  obtenerProductos() {
    this.productoService.getProductos().subscribe(data => {
      console.log(data);
      this.listProductos = data
    }, error => {
      console.log(error);
    })
  }

  obtenerProveedor() {
    this.proveedorService.getProveedores().subscribe(data => {
      console.log(data);
      this.listProveedores = data;
    }, error => {
      console.log(error);
    })
  }

  obtenerVentas(){
    this.vs.getVentas().subscribe(data =>{
      console.log(data);
      this.listVentas = data;
    }, error =>{
      console.log(error)
    })
  }

  obtenerCompras(){
    this.cs.getCompras().subscribe(data =>{
      console.log(data);
      this.listCompras = data;
    }, error =>{
      console.log(error)
    })
  }

  getCurrentUserRol(){
    const currentUser = JSON.parse(localStorage.getItem("currentUser")!);
    for (let i =0; i < currentUser.roles.length; i++){
        this.listRoles.push(currentUser.roles[i]);
    }
  }

  deleteProducto(id:String){
    this.productoService.eliminarProducto(id).subscribe(data=>{
      this.listProductos.splice(Number(id), 1)
    })
  }

  deleteProveedor(id:String){
    this.proveedorService.eliminarProveedor(id).subscribe(data=>{
      this.listProveedores.splice(Number(id), 1)
    })
  }


  hasPermission(){
    for (let i = 0; i < this.listRoles.length; i++) {
      if(this.listRoles[i].name == "admin") return true
    }
    return
  }

  cargarDatos(){
    if (this.hasPermission()) {
      this.obtenerProductos()
      this.obtenerProveedor()
    }
  }
}
