import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductoService } from 'src/app/services/producto.service';
import { CategoriaService } from 'src/app/services/categoria.service';
import { ProveedorService } from 'src/app/services/proveedor.service';
import { Categoria } from 'src/app/models/Categoria';
import { Proveedor } from '../../models/Proveedor';

@Component({
  selector: 'app-editarProducto',
  templateUrl: './editarProducto.component.html',
  styleUrls: ['./editarProducto.component.css']
})
export class EditarProductoComponent implements OnInit {
  
  producto: any ={}
  id:String =""

  constructor( private prs:ProveedorService, private cs:CategoriaService, private ps: ProductoService,private router:Router, private route:ActivatedRoute ) { }

  ngOnInit() {
    this.route.params.subscribe(params =>{
      this.ps.obtenerProducto(params['id']).subscribe(res =>{
        this.producto = res
      })
      this.id = params['id'];
    })

  }

  enviar() {
    this.ps.editarProducto(this.producto, this.id).subscribe(data =>{
      console.log(data)
      this.router.navigate( ['/inventario'] );
    }, error =>{
      console.log(error)
    })
  }


}