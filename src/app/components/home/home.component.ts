import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authService: AuthService, private router:Router ) { }

  ngOnInit() {
  }

  logout(){
    this.authService.logout()
  }

  compra() {
    this.router.navigate( ['/compra'] );
  }
  venta() {
    this.router.navigate( ['/venta'] );
  }
  inventario() {
    this.router.navigate( ['/inventario'] );
  }

}