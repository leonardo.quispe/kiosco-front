import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { ProveedorService } from 'src/app/services/proveedor.service';

@Component({
  selector: 'app-nuevoProv',
  templateUrl: './nuevoProv.component.html',
  styleUrls: ['./nuevoProv.component.css']
})
export class NuevoProvComponent implements OnInit {

  proveedor = {
    nombre: "",
    direccion: "",
    telefono: "",
    ruc: ""
  }

  constructor( private proveedorService: ProveedorService, private router:Router ) { }

  ngOnInit() {
  }

  enviar() {
    this.proveedorService.guardarProveedor(this.proveedor).subscribe(data =>{
      console.log(data);
      this.router.navigate( ['/inventario'] );
    }, error =>{
        console.log(error);
    });

  }

}