import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { VentaService } from 'src/app/services/venta.service';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-visualVenta',
  templateUrl: './visualVenta.component.html',
  styleUrls: ['./visualVenta.component.css']
})
export class VisualVentaComponent implements OnInit {

  venta: any ={}
  productos: any = {}
  id: String = ""
  page:number = 0;

  constructor( private ps: ProductoService,  private vs:VentaService, private router:Router, private route:ActivatedRoute ) { }

  ngOnInit() {
    this.cargarVenta();

  }

  nextPage(){
    this.page += 5;
  }

  prevPage(){
    if( this.page >0 )
     this.page -= 5;
  }


  cargarVenta(){
    this.route.params.subscribe(params =>{
      this.vs.obtenerVenta(params['id']).subscribe(res =>{
        this.venta = res
        this.productos = res.productos
        console.log(this.venta)
      })
        this.id = params['id'];
    })
  }

}