import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Producto } from '../../models/Producto';
import { Proveedor } from '../../models/Proveedor';
import { ProductoService } from 'src/app/services/producto.service';
import { NgForm } from '@angular/forms';
import { ProveedorService } from 'src/app/services/proveedor.service';
import { Categoria } from 'src/app/models/Categoria';
import { CategoriaService } from 'src/app/services/categoria.service';
import { NumberSymbol } from '@angular/common';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css'],
  providers: [ProductoService],
})
export class NuevoComponent implements OnInit {
  listProveedores: Proveedor[] = [];
  categorias: Categoria[] = [];
  
  producto = {
    name: "",
    categoria: "",
    cantidad: 0,
    costo_venta: 0,
    costo_compra: 0,
    proveedor: "",
  }

  constructor( private productoService: ProductoService, private proveedorService: ProveedorService, private categoriaService: CategoriaService, private router:Router ) { }

  ngOnInit() {
    this.cargardatos();
  }

  enviar() {
    this.productoService.guardarProducto(this.producto).subscribe((res)=>{
      console.log(res);
      this.router.navigate( ['/inventario'] );
    }, error => {
      console.log(error);
    })
  }

  cargardatos(){
    this.proveedorService.getProveedores().subscribe(data =>{
      this.listProveedores = data;
    }, error => {
      console.log(error);
    })
    
    this.categoriaService.getCategoria().subscribe(data =>{
      this.categorias = data
    }, error => {
      console.log(error);
    })
  }

}