import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { CompraService } from 'src/app/services/compra.service';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-visualCompra',
  templateUrl: './visualCompra.component.html',
  styleUrls: ['./visualCompra.component.css']
})
export class VisualCompraComponent implements OnInit {

  compra: any ={}
  productos: any = {}
  id: String = ""
  page:number = 0;

  constructor( private _location: Location, private ps: ProductoService, private cs:CompraService, private router:Router, private route:ActivatedRoute ) { }

  ngOnInit() {
    this.cargarCompra();
  }

  nextPage(){
    this.page += 5;
  }

  prevPage(){
    if( this.page >0 )
     this.page -= 5;
  }

  cargarCompra(){
    this.route.params.subscribe(params =>{
      this.cs.obtenerCompra(params['id']).subscribe(res =>{
        this.compra = res
        this.productos = res.productos
        console.log(this.compra)
      })
        this.id = params['id'];
    })
  }

  back(){
    this._location.back()
  }
}
