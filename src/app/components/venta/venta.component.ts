
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductoService } from 'src/app/services/producto.service';
import { VentaService } from 'src/app/services/venta.service';
import { Producto } from "../../models/Producto";

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent implements OnInit {

  productos: Producto[] = [];

  productos_detalle: Array<any> = [
  ];

  total: any = 0
  fecha!: Date 
  cantidades = {
    paga: 0,
    v: 0
  }

  formventa: Array<any> = [];

  venta = {
    producto: "",
    cantidad: 0,
    precio: 0,
  };

  constructor( private ps: ProductoService, private vs:VentaService, private router:Router ) { }

  ngOnInit() {
    this.cargarDatos();
  }

  cargarDatos(){
    this.ps.getProductos().subscribe(data => {
      console.log(data);
      this.productos = data;
    }, error => {
      console.log(error);
    })
  }

  validarCantidad(){
    let data = this.venta.producto.split("-");
    let existe = this.productos_detalle.findIndex(e=> e.producto_id == data [0]);
    if (this.venta.cantidad > Number(data[3]))
      return true
    return
  }

  price(){
    let data = this.venta.producto.split("-");
    this.venta.precio= Number(data[1]);
  }

  agregar(){
    let data = this.venta.producto.split("-");

    let existe = this.productos_detalle.findIndex(e=> e.producto_id == data [0]);
    
    if(existe != -1 ){
      this.productos_detalle[existe]={
        producto_id: data[0],
        producto_nombre: data[2],
        cantidad: Number(this.productos_detalle[existe].cantidad) + Number(this.venta.cantidad),
        precio: this.venta.precio,
        subtotal: Math.round(((Number(this.productos_detalle[existe].cantidad) + Number(this.venta.cantidad)) * this.venta.precio)*1e2)/1e2,
      }
    }else {
      this.productos_detalle.push({
        producto_id: data[0],
        producto_nombre: data[2],
        cantidad: this.venta.cantidad,
        precio: this.venta.precio,
        subtotal: Math.round((this.venta.cantidad * this.venta.precio)*1e2)/1e2,
      });
    }
    this.total += Math.round(((this.venta.cantidad) * Number(this.venta.precio))*1e2)/1e2;
    let ultimo = this.productos_detalle.length-1


    //this.formventa.productos_detalle = this.productos_detalle;
    //this.formventa.total = this.total;

  }

  vuelto(){
    this.cantidades.v = Math.round((Number(this.cantidades.paga) - Number(this.total))*1e2)/1e2;
  }

  guardarVenta(){
    this.formventa.push({
      fecha: this.fecha,
      productos_detalle: this.productos_detalle,
      total: this.total,
    })
    console.log(this.formventa);
    this.vs.guardarVenta(this.formventa[0]).subscribe(data =>{
      console.log(data)
      window.location.reload();
    }, error =>{
      console.log(error)
    })
  }

}