import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../../services/auth.service'
import { User } from '../../models/User';
import { Jsonp } from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = {
    email: "",
    password: "",
  };


  constructor(private authService: AuthService, private router:Router ) { }

  ngOnInit() {
  }

  signIn() {
    this.authService.signInUser(this.user)
      .subscribe(
        res => {
          console.log(res);
          localStorage.setItem('token', res.token);
          const currentUser = res.userFound;
          localStorage.setItem("currentUser", JSON.stringify(currentUser))
          this.router.navigate(['/home']);
        },
        err => console.log(err)
      )
  }
}