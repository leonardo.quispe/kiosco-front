import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProveedorService } from 'src/app/services/proveedor.service';

@Component({
  selector: 'app-editarProveedor',
  templateUrl: './editarProveedor.component.html',
  styleUrls: ['./editarProveedor.component.css']
})
export class EditarProveedorComponent implements OnInit {

  proveedor:any = {}
  id:String =""

  constructor( private prs:ProveedorService, private router:Router, private route:ActivatedRoute ) { }

  ngOnInit() {
    this.route.params.subscribe(params =>{
      this.prs.obtenerProveedor(params['id']).subscribe(res =>{
        this.proveedor = res
      })
      this.id = params['id'];
    })
  }

  enviar() {
    this.prs.editarProveedor(this.proveedor, this.id).subscribe(data =>{
      console.log(data)
      this.router.navigate( ['/inventario'] );
    }, error =>{
      console.log(error)
    })
  }

}