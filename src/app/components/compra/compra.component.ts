import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { Producto } from 'src/app/models/Producto';
import { CompraService } from 'src/app/services/compra.service';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  styleUrls: ['./compra.component.css']
})
export class CompraComponent implements OnInit {

  productos: Producto[] = [];

  productos_detalle: Array<any> = [
  ];

  total: any = 0
  fecha!: Date 
  cantidades = {
    paga: 0,
    v: 0
  }

  formcompra: Array<any> = [];

  compra = {
    producto: "",
    cantidad: 0,
    precio: 0,
  };

  constructor( private router:Router, private ps: ProductoService, private cs:CompraService ) { }

  ngOnInit() {
    this.cargarDatos();
  }

  cargarDatos(){
    this.ps.getProductos().subscribe(data => {
      console.log(data);
      this.productos = data;
    }, error => {
      console.log(error);
    })
  }

  price(){
    let data = this.compra.producto.split("-");
    this.compra.precio= Number(data[1]);
  }

  agregar(){
    let data = this.compra.producto.split("-");

    let existe = this.productos_detalle.findIndex(e=> e.producto_id == data [0]);
    
    if(existe != -1){
      this.productos_detalle[existe]={
        producto_id: data[0],
        producto_nombre: data[2],
        cantidad: Number(this.productos_detalle[existe].cantidad) + Number(this.compra.cantidad),
        precio: this.compra.precio,
        subtotal: Math.round(((Number(this.productos_detalle[existe].cantidad) + Number(this.compra.cantidad)) * this.compra.precio)*1e2)/1e2,
      }
    }else{
      this.productos_detalle.push({
        producto_id: data[0],
        producto_nombre: data[2],
        cantidad: this.compra.cantidad,
        precio: this.compra.precio,
        subtotal: Math.round((this.compra.cantidad * this.compra.precio)*1e2)/1e2,
      });
    }
    this.total += Math.round(((this.compra.cantidad) * Number(this.compra.precio))*1e2)/1e2;
  }

  guardarVenta(){
    this.formcompra.push({
      fecha: this.fecha,
      productos_detalle: this.productos_detalle,
      total: this.total,
    })
    console.log(this.formcompra);
    this.cs.guardarCompra(this.formcompra[0]).subscribe(data =>{
      console.log(data)
      window.location.reload();
    }, error =>{
      console.log(error)
    })
  }

}